<?php
declare(strict_types=1);

namespace QBNK\MarketoApi\Controller;

use GuzzleHttp\RequestOptions;
use QBNK\MarketoApi\Model\Folder;
use QBNK\MarketoApi\Model\FolderResponse;

class FolderController extends BaseController {

    protected $rootFolderId = '10092';
    /**
     * @param string|null $rootFolderId
     * @return FolderResponse[]
     */
    public function list(int $rootFolderId ): array {

        //TODO need to loop and page
            $queryParameters['root'] = "{ \"id\":".$rootFolderId.", \"type\":\"Folder\"}";
            $queryParameters['maxDepth'] = '20';
            $queryParameters['maxReturn'] = '200';
            $queryParameters['offset'] = '0';
            $queryParameters['workspace'] = '';


        $response = $this->apiHttpClient->get('/rest/asset/v1/folders.json', [RequestOptions::QUERY => $queryParameters]);
        $responseData = \GuzzleHttp\json_decode($response->getBody(), true);

        $result = [];

        foreach ($responseData['result'] as $rawFolder) {
            $result[] = FolderResponse::fromArray($rawFolder);
        }

        return $result;
    }

    /**
     * @param Folder $folder
     * @return FolderResponse
     */
    public function create(Folder $folder): FolderResponse {

        $queryParameters['parent'] = "{ \"id\":".$folder->getParentFolderId().", \"type\":\"Folder\"}";
        $queryParameters['name'] = $folder->getName();


        $response = $this->apiHttpClient->post('/rest/asset/v1/folders.json', [RequestOptions::QUERY => $queryParameters]);
        $responseData = \GuzzleHttp\json_decode($response->getBody(), true);
        $folderId = $responseData['result']['id'];
        return FolderResponse::fromArray( reset($responseData['result']) );
   }

    /**
     * TODO: Check API for get operation. This is not correct.
     * @param string $id
     * @return FolderResponse
     */
    public function get(string $id): FolderResponse {
        $response = $this->apiHttpClient->get('/rest/asset/v1/folders.json'.$id);
        $responseData = \GuzzleHttp\json_decode($response->getBody(), true);
        return FolderResponse::fromArray( reset($responseData['result']) );
    }

    /**
     * TODO: Check API for correct operation. This is not correct.
     * @param string $id
     * @return FolderResponse
     */
    public function update(string $id, Folder $folder): void {
        $this->apiHttpClient->put('folders/'.$id, ['json' => $folder]);
    }

    /**
     * TODO: Check API for correct operation. This is not correct.
     * @param string $id
     * @return FolderResponse
     */
    public function delete(string $id): void {
        $this->apiHttpClient->delete('/rest/asset/v1/folder/'.$id.'/delete.json');
    }
}