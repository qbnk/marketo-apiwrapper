<?php
declare(strict_types=1);

namespace QBNK\MarketoApi\Controller;


use GuzzleHttp\Client;

class BaseController {

    /** @var Client */
    protected $apiHttpClient;

    public function __construct($apiHttpClient) {
        $this->apiHttpClient = $apiHttpClient;
    }
}