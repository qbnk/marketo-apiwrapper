<?php

declare(strict_types=1);

namespace QBNK\MarketoApi\Controller;


use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\TransferException;
use GuzzleHttp\RequestOptions;
use QBNK\MarketoApi\Model\Image;
use QBNK\MarketoApi\Model\ImageResponse;

class ImageController extends BaseController {
    /**
     *
     * Sample query: https://509-APS-230.mktorest.com/rest/asset/v1/files.json?folder={ "id": 10092,"type": "Folder" }
     * @param string|null $folderId
     * @return ImageResponse[]
     */
    public function list(string $folderId = null ): array {
        $queryParameters = [];
        if ($folderId !== null) {
            $queryParameters['folder'] = "{ \"id\":".$folderId.", \"type\":\"Folder\" }";
            //$queryParameters['folder'] = "{ \"id\":10092, \"type\":\"Folder\"}";
        }

        $response = $this->apiHttpClient->get('/rest/asset/v1/files.json', [RequestOptions::QUERY => $queryParameters]);
        $responseData = \GuzzleHttp\json_decode($response->getBody(), true);

        $result = [];
        foreach ($responseData['result'] as $rawFolder) {
            $result[] = ImageResponse::fromArray($rawFolder);
        }

        return $result;
    }

    /**
     * File content needs to be in top of multipart (according to documentation, not tested)
     * File max size 100MB
     * Filename can't contain comma.
     * TODO Implement description property
     * @param Image $image
     * @param string $pathToFile
     * @return ImageResponse
     */
    public function create(Image $image, string $pathToFile): ImageResponse {



        $options = [
            'multipart' => [
                ['name' => 'filename', 'contents' => $image->getName()],
                ['name' => 'file', 'contents' => fopen($pathToFile, 'r')],
                ['name' => 'name', 'contents' => $image->getName()],
                ['name' => 'folder', 'contents' => "{ \"id\":".$image->getFolderId().", \"type\":\"Folder\" }"],
                ['name' => 'description', 'contents' => $image->getDescription() ],
                ['name' => 'insertOnly', 'contents' => 'false']

            ],
            'headers' => ['name' => 'Content-Type', 'contents' => 'text/plain' ] //Might need headers Content-Type text/plain
        ];

        $response = $this->apiHttpClient->post('/rest/asset/v1/files.json', $options);
        if ($response->getStatusCode() !== 200) {
            throw new TransferException('Non-successful response to upload: '.$response->getStatusCode().' '.$response->getReasonPhrase());
        }

        $responseData = \GuzzleHttp\json_decode($response->getBody(), true);

	    error_log( 'Create successful: ' . print_r($responseData['success'], true) );
        if ($responseData['success'] !== true) {
            //TODO File allready exist, publish again with uniqe filename.
            error_log( "Throwing exception in ImageController");
	        error_log( 'Non-successful response to upload: '.$responseData['errors']['id'].' '.$responseData['errors']['message'] );
            throw new TransferException('Non-successful response to upload: '.$responseData['errors']['id'].' '.$responseData['errors']['message']);

        }

        return ImageResponse::fromArray( reset($responseData['result']) );
    }

    public function get(string $id): ImageResponse {
        $response = $this->apiHttpClient->get('/images/'.$id);
        $responseData = \GuzzleHttp\json_decode($response->getBody(), true);
        return ImageResponse::fromArray($responseData);
    }

    public function update(string $id, Image $image, string $pathToFile): ImageResponse {
        $response = $this->apiHttpClient->patch('/rest/asset/v1/file/'.$id.'/content.json', [
            'multipart' => [
                ['name' => 'name', 'contents' => $image->getName()],
                ['name' => 'folderId', 'contents' => $image->getFolderId()],
                ['name' => 'tags', 'contents' => $image->getTags()],
                ['name' => 'externalData', 'contents' => $image->getExternalData()],
                ['name' => 'file', 'contents' => fopen($pathToFile, 'r')]
            ],
            'headers' => null
        ]);
        return $this->get($id);
    }

    public function delete(string $id): void {
        $this->apiHttpClient->delete('/images/'.$id);
    }

    /**
     * Retries $fn up to $retries times with exponential back off when 4xx responses are returned.
     * @param int $retries
     * @param callable $fn
     * @throws ClientException Thrown when $retries attempts has been made. Is the last real ClientException.
     * @return mixed The return value from $fn
     */
    protected function retryGuzzle(int $retries, callable $fn) {
        $attempts = 1;
        beginning:
        try {
            return $fn();
        } catch (ClientException $ce) {
            if (!$retries) {
                throw $ce;
            }
            $retries--;
            sleep(2 ** $attempts);   // Exponential back off
            $attempts++;
            goto beginning;
        }
    }

}
