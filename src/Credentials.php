<?php
declare(strict_types=1);

namespace QBNK\MarketoApi;

class Credentials
{
    /** @var string */
    protected $api_url;

    /** @var string */
    protected $clientId;

    /** @var string */
    protected $username;

    /**
     * @param string $clientId
     * @param string $clientSecret
     */
    public function __construct($api_url, $clientId, $clientSecret) {
        $this->api_url = $api_url;
        $this->clientId = $clientId;
        $this->clientSecret($clientSecret);
    }

    /**
     * Gets or sets the internal value
     * @internal Hack to hide value from dumping and possibly exposing by mistake.
     * @param string|null $newClientSecret
     * @return string
     */
    protected function clientSecret($newClientSecret = null): string {
        static $clientSecret;
        if ($newClientSecret !== null) {
            $clientSecret = $newClientSecret;
        }
        return $clientSecret ?? '';
    }

    public function getClientId(): string {
        return $this->clientId;
    }

    public function getClientSecret(): string {
        return $this->clientSecret();
    }

    /**
     * @return string
     */
    public function getApiUrl(): string
    {
        return $this->api_url;
    }

    /**
     * @param string $api_url
     * @return Credentials
     */
    public function setApiUrl(string $api_url): Credentials
    {
        $this->api_url = $api_url;
        return $this;
    }
}