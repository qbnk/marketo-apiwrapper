<?php
declare(strict_types=1);

namespace QBNK\MarketoApi;


use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\RequestOptions;
use QBNK\MarketoApi\Controller\FolderController;
use QBNK\MarketoApi\Controller\ImageController;
use QBNK\MarketoApi\OAuth\MarketoClientCredentials;
use Sainsburys\Guzzle\Oauth2\GrantType\ClientCredentials;
use Sainsburys\Guzzle\Oauth2\GrantType\RefreshToken;
use Sainsburys\Guzzle\Oauth2\Middleware\OAuthMiddleware;


class MarketoApi
{


    public const API_URL = 'https://not_used_check_config.com';
    public const API_TOKEN_URL = 'identity/oauth/token'; 

    /** @var Credentials */
    protected $credentials;

    /** @var Client */
    protected $apiClient;

    /** @var OAuthMiddleware */
    protected $oauth2Middleware;

    /** @var FolderController */
    protected $folderController;

    /** @var ImageController */
    protected $imageController;

    public function __construct( Credentials $credentials, array $options = []) {
        $this->credentials = $credentials;
    }

    public function getHttpClient(): Client {
        if (!($this->apiClient instanceof Client)) {
            $handlerStack = HandlerStack::create();

            $oauthClient = new Client([
                'base_uri' => $this->credentials->getApiUrl(),
                'verify' => true,
                'headers' => [
                    'User-Agent' => 'marketo-apiwrapper/1 (guzzle: 6)',
                ],
            ]);
            $config = [
                ClientCredentials::CONFIG_CLIENT_ID => $this->credentials->getClientId(),
                ClientCredentials::CONFIG_CLIENT_SECRET => $this->credentials->getClientSecret(),
                ClientCredentials::CONFIG_AUTH_LOCATION => RequestOptions::QUERY,
                //ClientCredentials::CONFIG_TOKEN_URL => self::API_URL."/".self::API_TOKEN_URL
                ClientCredentials::CONFIG_TOKEN_URL => $this->credentials->getApiUrl()."/".self::API_TOKEN_URL
            ];
            
            
            $this->oauth2Middleware = new OAuthMiddleware(
                $oauthClient,
                new MarketoClientCredentials($oauthClient, $config),
                new RefreshToken($oauthClient, $config)
            );
            $handlerStack->push($this->oauth2Middleware->onBefore());
            $handlerStack->push($this->oauth2Middleware->onFailure(3));


            $apiClient = new Client([
                'handler' => $handlerStack,
                'auth' => 'oauth2',
                'base_uri' => $this->credentials->getApiUrl().'/rest/assets/v1/folders.json',
                'headers' => [
                    'Accept' => 'application/json',
                    'Content-type' => 'application/json',
                    'User-Agent' => 'marketo-apiwrapper/1 (guzzle: 6)',
                ]
            ]);
            $this->apiClient = $apiClient;
        }

        return $this->apiClient;
    }

    public function folders(): FolderController {
        if (!($this->folderController instanceof FolderController)) {
            $this->folderController = new FolderController($this->getHttpClient());
        }
        return $this->folderController;
    }

    public function images(): ImageController {
        if (!($this->imageController instanceof ImageController)) {
            $this->imageController = new ImageController($this->getHttpClient());
        }
        return $this->imageController;
    }
}