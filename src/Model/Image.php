<?php
declare(strict_types=1);


namespace QBNK\MarketoApi\Model;


use JsonSerializable;


class Image implements JsonSerializable
{
    /** @var string */
    protected $name;

    /** @var string */
    protected $folderId;


    /** @var string */
    protected $description;

    /** @var string */
    protected $externalData;

    /** @var string */
    protected $id;

    /**
     * @return string
     */
    public function getId(): string {
        return $this->id;
    }

    /**
     * @param string $id
     * @return Image
     */
    protected function setId(string $id): Image {
        $this->id = $id;
        return $this;
    }

    /**
     * @param string $name
     * @return Image
     */
    public function setName(string $name): Image {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string {
        return $this->name;
    }

    /**
     * @param string $folderId
     * @return Image
     */
    public function setFolderId(string $folderId): Image {
        $this->folderId = $folderId;
        return $this;
    }

    /**
     * @return string
     */
    public function getFolderId(): string {
        return $this->folderId;
    }

    /**
     * TODO: Not sure if applicable for Marketo. Check and remove.
     * @param string $tags
     * @return Image
     */
    public function setTags(string $tags): Image {
        $this->tags = $tags;
        return $this;
    }

    /**
     * Not sure if applicable for Marketo. Check and remove.
     * @return string
     */
    public function getTags(): string {
        return $this->tags ?? '';
    }

    /**
     * @param string $externalData
     * @return Image
     */
    public function setExternalData(string $externalData): Image {
        $this->externalData = $externalData;
        return $this;
    }

    /**
     * @return string
     */
    public function getExternalData(): string {
        return $this->externalData ?? '';
    }

    /**
     * @return Image
     */
    public static function fromArray(array $data) {
        /** @var Image $instance */
        $instance = new static();
        $instance
            ->setId( (string)$data['id'] )
            ->setName((string)$data['name'])
            ->setDescription((string)$data['description'])
            ->setFolderId((string)$data['folder']['id'])
        ;
        return $instance;
    }

    /**
     * Specify data which should be serialized to JSON
     * @link https://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize() {

        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'folderId' => $this->getFolderId(),
            'description' => $this->getDescription(),
            'externalData' => $this->getExternalData()
        ];
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return Image
     */
    public function setDescription(string $description): Image
    {
        $this->description = $description;
        return $this;
    }
}
