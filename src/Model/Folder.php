<?php

namespace QBNK\MarketoApi\Model;


use JsonSerializable;

class Folder implements JsonSerializable {

    /** @var string */
    protected $name;

    /** @var string */
    protected $parentFolderId;

    /** @var string */

    protected $folderId;

    /** @var string  */

    protected $description;

    /**
     * @param string $name
     * @return self
     */
    public function setName(string $name) {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string {
        return $this->name ?? '';
    }

    /**
     * @param string $parentFolderId
     * @return self
     */
    public function setParentFolderId(string $parentFolderId) {
        $this->parentFolderId = $parentFolderId;
        return $this;
    }

    /**
     * @return string
     */
    public function getParentFolderId(): string {
        return $this->parentFolderId ?? '';
    }

    public static function fromArray(array $data) {
        $instance = new static();
        $instance
            ->setName($data['name'])
            ->setParentFolderId($data['parent']['id'])
            ->setFolderId($data['folderId']['id'])
           // ->setDescription( $data['description'] ?? '' )
        ;
        return $instance;
    }

    /**
     * Specify data which should be serialized to JSON
     * @link https://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize() {
        return [
            'name' => $this->getName(),
            'description' => $this->getDescription(),
            'folderId' => $this->getFolderId(),
            'parentFolderId' => $this->getParentFolderId()
        ];
    }

    /**
     * @return string
     */
    public function getFolderId(): string
    {
        return $this->folderId;
    }

    /**
     * @param string $folderId
     */
    public function setFolderId(string $folderId): void
    {
        $this->folderId = $folderId;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }
}