<?php

declare(strict_types=1);


namespace QBNK\MarketoApi\Model;


class ImageResponse extends Image
{

    /** @var string */
    protected $mimeType;

    /** @var int */
    protected $fileSize;

    /** @var string */
    protected $previewUrl;



    /**
     * @return string
     */
    public function getMimeType(): string {
        return $this->mimeType;
    }


    /**
     * @return int
     */
    public function getFileSize(): int {
        return $this->fileSize;
    }

    /**
     * @return string
     */
    public function getPreviewUrl(): string {
        return $this->previewUrl;
    }



    /**
     * @param string $mimeType
     * @return ImageResponse
     */
    protected function setMimeType(string $mimeType): ImageResponse {
        $this->mimeType = $mimeType;
        return $this;
    }


    /**
     * @param int $fileSize
     * @return ImageResponse
     */
    protected function setFileSize(int $fileSize): ImageResponse {
        $this->fileSize = $fileSize;
        return $this;
    }


    /**
     * @param string $previewUrl
     * @return ImageResponse
     */
    protected function setPreviewUrl(string $previewUrl): ImageResponse {
        $this->previewUrl = $previewUrl;
        return $this;
    }


    /**
     * TODO Are missing file attributes needed for qbank?
     * @return ImageResponse
     */
    public static function fromArray(array $data) {
        /** @var  $instance ImageResponse */
        $instance = parent::fromArray($data);
        $instance
            ->setId((string)$data['id'])
            ->setMimeType((string)$data['mimeType'])
            ->setFileSize((int)$data['size'])
            ->setPreviewUrl((string)$data['url'])
            
        ;
        return $instance;
    }
}