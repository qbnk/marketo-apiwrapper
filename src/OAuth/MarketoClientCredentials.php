<?php


namespace QBNK\MarketoApi\OAuth;


use GuzzleHttp\RequestOptions;
use Sainsburys\Guzzle\Oauth2\AccessToken;
use Sainsburys\Guzzle\Oauth2\GrantType\ClientCredentials;
use Sainsburys\Guzzle\Oauth2\GrantType\GrantTypeBase;
use GuzzleHttp\Client;

class MarketoClientCredentials extends ClientCredentials
{
    protected $grantType = 'client_credentials';

    /* @var $client Client */

    /**
     * {@inheritdoc}
     */
    public function getToken()
    {
        $body = $this->config;
        $body[self::GRANT_TYPE] = $this->grantType;
        unset($body[self::CONFIG_TOKEN_URL], $body[self::CONFIG_AUTH_LOCATION]);

        $requestOptions = [];

        if ($this->config[self::CONFIG_AUTH_LOCATION] !== RequestOptions::BODY) {
            $requestOptions[RequestOptions::AUTH] = [
                $this->config[self::CONFIG_CLIENT_ID],
                isset($this->config[self::CONFIG_CLIENT_SECRET]) ? $this->config[self::CONFIG_CLIENT_SECRET] : '',
            ];
            //unset($body[self::CONFIG_CLIENT_ID], $body[self::CONFIG_CLIENT_SECRET]);
        }

        unset($body["scope"]);
        $requestOptions[RequestOptions::QUERY] = $body;


        if ($additionalOptions = $this->getAdditionalOptions()) {
            $requestOptions = array_merge_recursive($requestOptions, $additionalOptions);
        }


        $response = $this->client->get($this->config[self::CONFIG_TOKEN_URL], $requestOptions);

        $data = json_decode($response->getBody()->__toString(), true);

        return new AccessToken($data['access_token'], $data['token_type'], $data);
    }

}