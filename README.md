# Marketo Api Wrapper

A simple API wrapper for Marketo https://www.marketo.com Allows for simple communication from PHP with Marketo with 
minimal setup.

## Requirements
 * PHP 7.2+
 * Guzzle 6.1+

## Installation
`composer require qbnk/marketo-apiwrapper ^0.1`

## Authorization
Marketo's API uses OAuth2 with the client credentials flow. This means that you will need a *client id* and a *client 
secret* to be able to authorize with and use the API. The *client id* and *client secret* is available in the 
Marketo administration.

### Example
To create an instance that can authorize with the API, simply pass a `Credentials` instance with the *client id* and 
*client secret* to the `MarketoApi` constructor. The library will then handle the rest.
```php
$api = new MarketoApi(new Credentials('customer', 'client secret'));
``` 

## FAQ
